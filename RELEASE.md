# ADSIS8300 Releases

GIT : https://gitlab.esss.lu.se/beam-diagnostics/bde/epics/modules/adsis8300

# Release Notes

## R1-3 (October XXX, 2018)

* driver provides 10 AI channels of data
* each samples is UIn16 bit value
* ..
