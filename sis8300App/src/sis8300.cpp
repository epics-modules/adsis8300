/* sis8300.cpp
 *
 * This is a driver for a Struck SIS8300 digitizer.
 * Based on ADCSimDetector ADExample.
 *
 * Author: Hinko Kocevar
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  June 12, 2018
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <string>

#include <dbAccess.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <iocsh.h>

#include <asynNDArrayDriver.h>
#include <epicsExport.h>

#include <sis8300.h>

static const char *driverName = "sis8300";

// exit handler, delete the sis8300 object
static void exitHandler(void *drvPvt) {
    sis8300 *pPvt = (sis8300 *) drvPvt;
    delete pPvt;
}

static void sisTaskC(void *drvPvt)
{
    sis8300 *pPvt = (sis8300 *)drvPvt;
    pPvt->sisTask();
}

// user-defined time stamp source callback invoked on updateTimeStamp() call
static void evrTimeStampSource(void *drvPvt, epicsTimeStamp *pTimeStamp)
{
    sis8300 *pPvt = (sis8300 *)drvPvt;

    // if TimestampLink.DOL PV value is not set, time stamp will be 0.0
    // if TimestampLink.DOL PV is set and then unset, Timestamp PV should
    // be set to 0.0 manually to make this work
    if (pPvt->mTimeStampSec && pPvt->mTimeStampNsec) {
        // time stamp obtained from the EVR
        pTimeStamp->secPastEpoch = pPvt->mTimeStampSec;
        pTimeStamp->nsec = pPvt->mTimeStampNsec;
    } else {
        // default time stamp source
        epicsTimeGetCurrent(pTimeStamp);
    }
}

/** Constructor for sis8300; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method creates a thread to compute the simulated detector data,
  * and sets reasonable default values for parameters defined in this class, asynNDArrayDriver and ADDriver.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] devicePath The path to the /dev entry (usually /dev/sis8300-<AMC slot>).
  * \param[in] numSamples The initial number of AI samples.
  * \param[in] extraPorts Additinonal number of asyn ports to register. Used for class that extend this class.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
sis8300::sis8300(const char *portName, const char *devicePath,
        int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
        int priority, int stackSize)

    : asynNDArrayDriver(portName,
            SIS8300DRV_NUM_AI_CHANNELS + extraPorts,
            maxBuffers, maxMemory,
            asynFloat32ArrayMask | asynFloat64ArrayMask | asynUInt32DigitalMask,
            asynFloat32ArrayMask | asynFloat64ArrayMask | asynUInt32DigitalMask,
            ASYN_CANBLOCK | ASYN_MULTIDEVICE,
            1,
            priority,
            stackSize),
            mUniqueId(0), mAcquiring(0)

{
    asynPrintDeviceInfo(pasynUserSelf, "we have %d asyn addresses", maxAddr);

    // create an EPICS exit handler
    epicsAtExit(exitHandler, this);

    snprintf(mDevicePath, MAX_PATH_LENGTH, "%s", devicePath);
    mDeviceHandle = (sis8300drv_usr*)calloc(1, sizeof(sis8300drv_usr));
    mDeviceHandle->file = mDevicePath;

    // create the epicsEvents for signaling to the acquisition
    // task when acquisition starts and stops
    mStartEventId = epicsEventCreate(epicsEventEmpty);
    if (! mStartEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate failure for start event");
    }
    mStopEventId = epicsEventCreate(epicsEventEmpty);
    if (! mStopEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate failure for stop event");
    }

    // system parameters
    createParam(SIS8300NumSamplesString,                    asynParamInt32,     &SIS8300NumSamples);
    createParam(SIS8300ClockSourceString,                   asynParamInt32,     &SIS8300ClockSource);
    createParam(SIS8300ClockDividerString,                  asynParamInt32,     &SIS8300ClockDivider);
    createParam(SIS8300TriggerSourceString,                 asynParamInt32,     &SIS8300TriggerSource);
    createParam(SIS8300TriggerLineString,                   asynParamInt32,     &SIS8300TriggerExternalLine);
    createParam(SIS8300TriggerDelayString,                  asynParamInt32,     &SIS8300TriggerDelay);
    createParam(SIS8300TriggerRepeatString,                 asynParamInt32,     &SIS8300TriggerRepeat);
    createParam(SIS8300IrqPollString,                       asynParamInt32,     &SIS8300IrqPoll);
    createParam(SIS8300ResetString,                         asynParamInt32,     &SIS8300Reset);
    createParam(SIS8300DriverMessageString,                 asynParamOctet,     &SIS8300DriverMessage);
    createParam(SIS8300MemorySizeString,                    asynParamInt32,     &SIS8300MemorySize);
    createParam(SIS8300DevicePathString,                    asynParamOctet,     &SIS8300DevicePath);
    createParam(SIS8300RtmTypeString,                       asynParamInt32,     &SIS8300RtmType);
    createParam(SIS8300SamplingFrequencyString,             asynParamFloat64,   &SIS8300SamplingFrequency);
    createParam(SIS8300TickValueString,                     asynParamFloat64,   &SIS8300TickValue);
    createParam(SIS8300BeamModeSourceString,                asynParamInt32,     &SIS8300BeamModeSource);
    createParam(SIS8300BeamModeValueString,                 asynParamInt32,     &SIS8300BeamModeValue);
    createParam(SIS8300BeamDestinationSourceString,         asynParamInt32,     &SIS8300BeamDestinationSource);
    createParam(SIS8300BeamDestinationValueString,          asynParamInt32,     &SIS8300BeamDestinationValue);
    createParam(SIS8300PcieLinkSpeedString,                 asynParamInt32,     &SIS8300PcieLinkSpeed);
    createParam(SIS8300PcieLinkWidthString,                 asynParamInt32,     &SIS8300PcieLinkWidth);
    createParam(SIS8300PcieTransferTimeString,              asynParamFloat64,   &SIS8300PcieTransferTime);
    createParam(SIS8300PcieStartTimeString,                 asynParamFloat64,   &SIS8300PcieStartTime);
    createParam(SIS8300PcieEndTimeString,                   asynParamFloat64,   &SIS8300PcieEndTime);
    createParam(SIS8300TransferArraysString,                asynParamInt32,     &SIS8300TransferArrays);
    createParam(SIS8300TriggerTimeoutString,                asynParamInt32,     &SIS8300TriggerTimeout);
    createParam(SIS8300TriggerTimeoutValueString,           asynParamInt32,     &SIS8300TriggerTimeoutValue);
    createParam(SIS8300ADCRangeString,                      asynParamInt32,     &SIS8300ADCRange);
    createParam(SIS8300ConvertToVoltageString,              asynParamInt32,     &SIS8300ConvertToVoltage);
    // EVR parameters
    createParam(SIS8300EvrTimestampString,                  asynParamOctet,     &SIS8300EvrTimestamp);
    createParam(SIS8300EvrBeamStateString,                  asynParamInt32,     &SIS8300EvrBeamState);
    createParam(SIS8300EvrLinkString,                       asynParamInt32,     &SIS8300EvrLink);
    createParam(SIS8300EvrLinkStatusString,                 asynParamInt32,     &SIS8300EvrLinkStatus);
    createParam(SIS8300EvrLinkSeverityString,               asynParamInt32,     &SIS8300EvrLinkSeverity);
    // channel parameters
    createParam(SIS8300ChControlString,                     asynParamInt32,     &SIS8300ChControl);
    createParam(SIS8300ChAttenuationString,                 asynParamFloat64,   &SIS8300ChAttenuation);
    createParam(SIS8300ChInternalTriggerLengthString,       asynParamInt32,     &SIS8300ChInternalTriggerLength);
    createParam(SIS8300ChInternalTriggerConditionString,    asynParamInt32,     &SIS8300ChInternalTriggerCondition);
    createParam(SIS8300ChInternalTriggerOffString,          asynParamInt32,     &SIS8300ChInternalTriggerOff);
    createParam(SIS8300ChInternalTriggerOnString,           asynParamInt32,     &SIS8300ChInternalTriggerOn);
    createParam(SIS8300ChPcieTransferTimeString,            asynParamFloat64,   &SIS8300ChPcieTransferTime);
    createParam(SIS8300ChPcieStartTimeString,               asynParamFloat64,   &SIS8300ChPcieStartTime);

    // this is digitizer native sample format
    setIntegerParam(NDDataType, NDUInt16);
    // maximum number of samples we can ask for, per channel
    // setIntegerParam(SIS8300NumSamples, numSamples);
    setStringParam(ADManufacturer, "Struck");
    setStringParam(SIS8300DevicePath, mDevicePath);
    setStringParam(SIS8300EvrTimestamp, "");

    // register our time stamp source function that will set time stamp obtained
    // from EVR
    pasynManager->registerTimeStampSource(pasynUserSelf, this, evrTimeStampSource);

    // create a joinable thread that acquires the data from the device
    mExitAcqThread = false;
    epicsThreadOpts opts = EPICS_THREAD_OPTS_INIT;
    opts.priority = epicsThreadPriorityHigh;
    opts.stackSize = epicsThreadGetStackSize(epicsThreadStackMedium);
    opts.joinable = 1;
    mAcqThreadId = epicsThreadCreateOpt("SisTask",
                                (EPICSTHREADFUNC)sisTaskC,
                                this,
                                &opts);
    if (! mAcqThreadId) {
        asynPrintError(pasynUserSelf, "epicsThreadCreateOpt failure for acquisition task");
        return;
    }

    int ret = initDevice();
    if (ret) {
        asynPrintError(pasynUserSelf, "initialization FAILED");
        setStringParam(SIS8300DriverMessage, "initialization FAILED");
    } else {
        asynPrintDeviceInfo(pasynUserSelf, "initialization PASSED");
        setStringParam(SIS8300DriverMessage, "initialization PASSED");
    }
}

sis8300::~sis8300() {
    asynPrintDeviceInfo(pasynUserSelf, "shutting down..");

    // stop the acquisition thread
    mExitAcqThread = true;
    setAcquire(0);

    lock();
    epicsEventTrigger(mStartEventId);
    unlock();
    epicsThreadMustJoin(mAcqThreadId);

    destroyDevice();
    free(mDeviceHandle);
    asynPrintDeviceInfo(pasynUserSelf, "shutdown complete!");
}

int sis8300::initDevice()
{
    int ret = sis8300drv_open_device(mDeviceHandle);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_open_device returned %d", ret);
        return ret;
    }

    unsigned int serialNumber;
    ret = sis8300drv_get_serial(mDeviceHandle, &serialNumber);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_serial returned %d", ret);
        return ret;
    }
    char buf[256];
    snprintf(buf, 256, "%d", serialNumber);
    setStringParam(ADSerialNumber, buf);

    unsigned int firmwareVersion;
    ret = sis8300drv_get_fw_version(mDeviceHandle, &firmwareVersion);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_fw_version returned %d", ret);
        return ret;
    }
    firmwareVersion &= 0x0000FFFF;
    snprintf(buf, 256, "%04X", firmwareVersion);
    setStringParam(ADFirmwareVersion, buf);

    unsigned int deviceType;
    ret = sis8300drv_get_device_type(mDeviceHandle, &deviceType);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_device_type returned %d", ret);
        return ret;
    }

    unsigned long memorySizeMb;
    ret = sis8300drv_get_memory_size(mDeviceHandle, &memorySizeMb);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_memory_size returned %d", ret);
        return ret;
    }
    memorySizeMb /= (1024*1024);

    ret = sis8300drv_init_adc(mDeviceHandle);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_init_adc returned %d", ret);
        return ret;
    }

    setIntegerParam(SIS8300MemorySize, memorySizeMb);
    switch (deviceType) {
    case SIS8300_SIS8300:
        snprintf(buf, 256, "SIS8300");
        break;
    case SIS8300_SIS8300L:
        snprintf(buf, 256, "SIS8300-L");
        break;
    case SIS8300_SIS8300L2:
        snprintf(buf, 256, "SIS8300-L2");
        break;
    case SIS8300_SIS8300KU:
        snprintf(buf, 256, "SIS8300-KU");
        break;
    default:
        snprintf(buf, 256, "Unknown");
        break;
    }
    setStringParam(ADModel, buf);

    printf("Port name        : %s\n", portName);
    getStringParam(SIS8300DevicePath, 256, buf);
    printf("Device path      : %s\n", buf);
    getStringParam(ADManufacturer, 256, buf);
    printf("Manufacturer     : %s\n", buf);
    getStringParam(ADFirmwareVersion, 256, buf);
    printf("Firmware version : %s\n", buf);
    getStringParam(ADSerialNumber, 256, buf);
    printf("Serial number    : %s\n", buf);
    getStringParam(ADModel, 256, buf);
    printf("Model            : %s\n", buf);
    getIntegerParam(SIS8300MemorySize, (int *)&memorySizeMb);
    printf("Memory size      : %d MB\n", (int)memorySizeMb);

    // first time read back the firmware register values
    ret = readbackParameters();

    return ret;
}

int sis8300::destroyDevice()
{
    int ret = sis8300drv_close_device(mDeviceHandle);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_close_device returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300::initDeviceDone()
{
    return 0;
}

int sis8300::armDevice()
{
    int ret = sis8300drv_arm_device(mDeviceHandle);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_arm_device returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300::disarmDevice()
{
    int ret = sis8300drv_disarm_device(mDeviceHandle);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_disarm_device returned %d", ret);
        return ret;
    }
    ret = sis8300drv_release_irq(mDeviceHandle, irq_type_usr);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_release_irq returned %d", ret);
        return ret;
    }
    ret = sis8300drv_release_irq(mDeviceHandle, irq_type_daq);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_release_irq returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300::waitForDevice()
{
    int irqPoll;
    getIntegerParam(SIS8300IrqPoll, &irqPoll);

    int ret;
    if (irqPoll) {
        // for DAQ end since there will be not interrupt in this mode
        ret = sis8300drv_poll_acq_end(mDeviceHandle);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drv_poll_acq_end returned %d", ret);
            return ret;
        }
    } else {
        int timeoutValue;
        getIntegerParam(SIS8300TriggerTimeoutValue, &timeoutValue);
        // wait for DAQ interrupt since interrupt will be generated
        ret = sis8300drv_wait_acq_end(mDeviceHandle, timeoutValue);
        if (ret) {
            // do not print error on timeout as we do not treat that as an error
            if (ret != status_irq_timeout) {
                asynPrintError(pasynUserSelf, "sis8300drv_wait_acq_end returned %d", ret);
            }
            return ret;
        }
    }

    return ret;
}

// perform (read-only) FPGA register readout which update *after* the acquisition ends
// note that user (read/write) FPGA register readback is performed *before* the acquisition start
int sis8300::deviceDone()
{
    // record the time of interrupt arrival (acquisition done)
    epicsTimeGetCurrent(&mInterruptTs);

    return 0;
}

int sis8300::setupChannelMask(int addr)
{
    unsigned int channelMask;
    int ret = sis8300drv_get_channel_mask(mDeviceHandle, &channelMask);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_channel_mask returned %d", ret);
        return ret;
    }
    int state;
    getIntegerParam(addr, SIS8300ChControl, &state);
    if (state) {
        channelMask |= (1 << addr);
    } else {
        channelMask &= ~(1 << addr);
    }
    ret = sis8300drv_set_channel_mask(mDeviceHandle, channelMask);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_set_channel_mask returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300::setupInternalTrigger(int addr)
{
    int trigSrc;
    getIntegerParam(SIS8300TriggerSource, &trigSrc);
    // enable if trigger source is set to internal, disable otherwise
    int intTrigEnable = 0;
    if ((sis8300drv_trg_src)trigSrc == trg_src_internal) {
        intTrigEnable = 1;
    }
    int thrOff, thrOn, cond, len;
    getIntegerParam(addr, SIS8300ChInternalTriggerLength, &len);
    getIntegerParam(addr, SIS8300ChInternalTriggerCondition, &cond);
    getIntegerParam(addr, SIS8300ChInternalTriggerOff, &thrOff);
    getIntegerParam(addr, SIS8300ChInternalTriggerOn, &thrOn);
    thrOff &= 0xFFFF;
    thrOn &= 0xFFFF;
    cond &= 0x1;
    len &= 0xFF;
    int trigEnable = (intTrigEnable && (len > 0));
    // setup internal triggering parameters (use threshold triggering mode)
    // NOTE: Only threshold triggering is supported at the moment, FIR filter
    //       is not handled.
    int ret = sis8300drv_set_internal_setup(mDeviceHandle, addr, trigEnable, 0,
            thrOff << 16 | thrOn, cond, len, 0, 0);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_set_channel_mask returned %d", ret);
        return ret;
    }

    return ret;
}

void sis8300::updateTickValue()
{
    // Handle time axis time tick updates that depend on:
    //    - sampling frequency
    //    - clock source

    int clockSrc, clockDiv;
    double samplingFreq;
    getIntegerParam(SIS8300ClockSource, &clockSrc);
    getIntegerParam(SIS8300ClockDivider, &clockDiv);
    getDoubleParam(SIS8300SamplingFrequency, &samplingFreq);

    // if we are using internal clock derive sampling frequency
    // otherwise we have to use SamplingFreq value from PV (in Hz)
    if ((sis8300drv_clk_src)clockSrc == clk_src_internal) {
        samplingFreq = 250.0 * 1e6 / clockDiv;
        setDoubleParam(SIS8300SamplingFrequency, samplingFreq);
    }
    asynPrintDeviceInfo(pasynUserSelf, "using sampling frequency %g", samplingFreq);
    double tick = 1.0 / samplingFreq;
    setDoubleParam(SIS8300TickValue, tick);
    asynPrintDeviceInfo(pasynUserSelf, "time axis tick set to %g s", tick);
}

// perform (read/write) FPGA register readback *before* the acquisition start
// note that (read-only) FPGA register readout is performed *after* the acquisition ends
int sis8300::readbackParameters()
{
    unsigned int uintValue;
    int ret = sis8300drv_get_nsamples(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_nsamples returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300NumSamples, uintValue);

    ret = sis8300drv_get_channel_mask(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_channel_mask returned %d", ret);
        return ret;
    }
    for (int addr = 0; addr < SIS8300DRV_NUM_AI_CHANNELS; addr++) {
        setIntegerParam(addr, SIS8300ChControl, (uintValue & (1 << addr)) ? 1 : 0);
    }

    ret = sis8300drv_get_npretrig(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_npretrig returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300TriggerDelay, uintValue);

    ret = sis8300drv_get_clock_source(mDeviceHandle, (sis8300drv_clk_src *)&uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_clock_source returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300ClockSource, uintValue);

    ret = sis8300drv_get_clock_divider(mDeviceHandle, (sis8300drv_clk_div *)&uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_clock_divider returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300ClockDivider, uintValue);

    ret = sis8300drv_get_trigger_source(mDeviceHandle, (sis8300drv_trg_src *)&uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_trigger_source returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300TriggerSource, uintValue);

    unsigned int uintValue2;
    ret = sis8300drv_get_pcie_status(mDeviceHandle, &uintValue, &uintValue2);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drv_get_pcie_status returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300PcieLinkSpeed, uintValue);
    setIntegerParam(SIS8300PcieLinkWidth, uintValue2);

    // XXX add missing readbacks here..

    // int sis8300drv_get_external_setup(sis8300drv_usr *sisuser,
    //         sis8300drv_trg_ext trg_ext, unsigned *trg_mask, unsigned *edge_mask) {

    // int sis8300drv_get_internal_setup(sis8300drv_usr *sisuser, unsigned channel,
    //         unsigned *enable, unsigned *mode, unsigned *threshold, unsigned *condition,
    //         unsigned *pulse_length, unsigned *sum_gap, unsigned *peaking_time) {

    callParamCallbacks();

    return 0;
}

bool sis8300::checkEvrLink()
{
    int evrLink;
    getIntegerParam(SIS8300EvrLink, &evrLink);
    // Link-Sts EVR PV value 1 means link is up
    if (evrLink != 1) {
        return false;
    }

    int evrLinkStatus;
    getIntegerParam(SIS8300EvrLinkStatus, &evrLinkStatus);
    // Link-Sts.STAT EVR PV value 0 means no alarms
    if (evrLinkStatus != 0) {
        return false;
    }

    int evrLinkSeverity;
    getIntegerParam(SIS8300EvrLinkSeverity, &evrLinkSeverity);
    // Link-Sts.SEVR EVR PV value 0 means no alarms
    if (evrLinkSeverity != 0) {
        return false;
    }

    return true;
}

int sis8300::acquireRawArrays()
{
    int numSamples;
    getIntegerParam(SIS8300NumSamples, &numSamples);
    size_t dims[1];
    dims[0] = numSamples;
    epicsTimeStamp tNow, tEnd, tStart;
    double elapsed, total;

    total = 0;
    // reset PCIe times
    for (int addr = 0; addr < SIS8300DRV_NUM_AI_CHANNELS; addr++) {
        setDoubleParam(addr, SIS8300ChPcieTransferTime, 0.0);
        setDoubleParam(addr, SIS8300ChPcieStartTime, 0.0);
    }
    setDoubleParam(SIS8300PcieTransferTime, 0.0);
    // record PCIe start time (delay after receiving the interrupt)
    epicsTimeGetCurrent(&tStart);
    elapsed = epicsTimeDiffInSeconds(&tStart, &mInterruptTs);
    setDoubleParam(SIS8300PcieStartTime, elapsed * 1000.0);
    setDoubleParam(SIS8300PcieEndTime, 0.0);
    int doTransfer;
    getIntegerParam(SIS8300TransferArrays, &doTransfer);
    int adcRange;
    getIntegerParam(SIS8300ADCRange, &adcRange);
    int convertToVoltage;
    getIntegerParam(SIS8300ConvertToVoltage, &convertToVoltage);
    // conversion from ADC counts to volts is as follows (for 2 V range):
    //   * volts per ADC = 2 V / 65536 = 0.0000305176 V (30.5176 uV)
    //   * offset is always 32768 ADC counts (half ADC range)
    // volts = (raw - offset) * factor

    // by default do not perform the conversion to volts, but only subtract
    // the ADC implied offset of half ADC range
    double convFactor = 1.0;
    int convOffset = 32768;
    if (convertToVoltage) {
        convFactor = (double)adcRange / 65536;
    }

    // AI channels are at asyn addr 0 .. 9
    for (int addr = 0; addr < SIS8300DRV_NUM_AI_CHANNELS; addr++) {
        if (pArrays[addr]) {
            pArrays[addr]->release();
        }
        pArrays[addr] = NULL;

        // check if we need to transfer the channel data
        if (! doTransfer) {
            continue;
        }

        int chState;
        getIntegerParam(addr, SIS8300ChControl, &chState);
        if (! chState) {
            continue;
        }

        // record PCIe start time of this channel
        epicsTimeGetCurrent(&tNow);
        elapsed = epicsTimeDiffInSeconds(&tNow, &tStart);
        setDoubleParam(addr, SIS8300ChPcieStartTime, elapsed * 1000.0);

        asynPrintDeviceInfo(pasynUserSelf, "AI addr %d, max samples %d", addr, numSamples);
        NDArray *pArray = pNDArrayPool->alloc(1, dims, NDUInt16, 0, 0);
        epicsUInt16 *pData = (epicsUInt16 *)pArray->pData;

        int ret = sis8300drv_read_ai(mDeviceHandle, addr, pData);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drv_read_ai returned %d", ret);
            return ret;
        }

        // always deliver floating point samples
        NDArray *pArray2 = pNDArrayPool->alloc(1, dims, NDFloat32, 0, 0);
        epicsFloat32 *pData2 = (epicsFloat32 *)pArray2->pData;
        for (int i = 0; i < numSamples; i++) {
            pData2[i]= (pData[i] - convOffset) * convFactor;
        }
        pArrays[addr] = pArray2;
        pArray->release();

        // record PCIe transfer time of this channel
        epicsTimeGetCurrent(&tEnd);
        elapsed = epicsTimeDiffInSeconds(&tEnd, &tNow);
        asynPrintDeviceInfo(pasynUserSelf, "channel %d, PCIe elapsed %f, total %f\n", addr, elapsed, total);
        setDoubleParam(addr, SIS8300ChPcieTransferTime, elapsed * 1000.0);
        // update PCIe total time
        total += elapsed;
    }

    // record the PCIe end time
    epicsTimeGetCurrent(&tEnd);
    elapsed = epicsTimeDiffInSeconds(&tEnd, &mInterruptTs);
    setDoubleParam(SIS8300PcieEndTime, elapsed * 1000.0);

    // record the PCIe total time (sum of all channels)
    asynPrintDeviceInfo(pasynUserSelf, "PCIe total %f ms\n", total * 1000.0);
    setDoubleParam(SIS8300PcieTransferTime, total * 1000.0);
    if ((total * 1000.0) > 50.0) {
        asynPrintError(pasynUserSelf, "WARNING! PCIe total %f ms\n", total * 1000.0);
    }

    return 0;
}

int sis8300::acquireArrays()
{
    return acquireRawArrays();
}

void sis8300::setAcquire(int value)
{
    if (value && !mAcquiring) {
        // send an event to start the acquisition
        epicsEventSignal(mStartEventId);
    }
    if (!value && mAcquiring) {
        // send an event to stop the acquisition
        epicsEventSignal(mStopEventId);
    }
}

void sis8300::sisTask()
{
    asynPrintDeviceInfo(pasynUserSelf, "waiting for iocInit to finish..");
    while (! interruptAccept) {
        epicsThreadSleep(0.1);
    }
    asynPrintDeviceInfo(pasynUserSelf, "data thread start");

    int status = asynSuccess;
    int arrayCounter;
    int triggerRepeat;
    int triggerCount = 0;
    int ret;
    int chState;
    int numChEnabled;

    lock();
    while (1) {
taskStart:
        // acquisition might have been stopped by the user
        status = epicsEventTryWait(mStopEventId);
        if (status == epicsEventWaitOK) {
            asynPrintDeviceInfo(pasynUserSelf, "stop event detected!");
            mAcquiring = 0;
        }

        if (mExitAcqThread) {
            // exit the thread
            unlock();
            break;
        }

        // if we are not acquiring then wait
        if (! mAcquiring) {
            disarmDevice();
            setIntegerParam(ADAcquire, 0);

            for (int addr = 0; addr < maxAddr; addr++) {
                callParamCallbacks(addr);
            }

            asynPrintDeviceInfo(pasynUserSelf, "waiting for start event..");
            // release the lock while we wait for the start event
            unlock();
            status = epicsEventWait(mStartEventId);
            lock();
            asynPrintDeviceInfo(pasynUserSelf, "start event arrived!");

            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "");
            setStringParam(SIS8300DriverMessage, mMessageBuffer);

            mAcquiring = 1;
            triggerCount = 0;

            ret = initDeviceDone();
            if (ret) {
                mAcquiring = 0;
                goto taskStart;
            }
        }

        if (mExitAcqThread) {
            // exit the thread
            unlock();
            break;
        }

        ret = sis8300drv_is_device_open(mDeviceHandle);
        if (! ret) {
            asynPrintError(pasynUserSelf, "device %s not opened!", mDevicePath);
            mAcquiring = 0;
            goto taskStart;
        }

        // count enabled channels
        numChEnabled = 0;
        for (int a = 0; a < maxAddr; a++) {
            getIntegerParam(a, SIS8300ChControl, &chState);
            if (chState) {
                numChEnabled++;
            }
        }
        // at least one channel needs to be enabled
        asynPrintDeviceInfo(pasynUserSelf, "Have %d channels enabled", numChEnabled);
        if (numChEnabled == 0) {
            asynPrintError(pasynUserSelf, "No channels enabled, stopping acquisition!");
            mAcquiring = 0;
            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "No channels enabled, stopping acquisition!");
            setStringParam(SIS8300DriverMessage, mMessageBuffer);
            goto taskStart;
        }

        ret = armDevice();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }
        callParamCallbacks(0);

        // unlock while waiting for the device
        unlock();
        // call to this function will block and wait for device to
        // signal acquisition finished condition (interrupt or poll)
        // or timeout on while waiting (not an error!)
        ret = waitForDevice();

        // update the timestamp with value from EVR
        updateTimeStamp();

        // lock it back
        lock();
        if (ret) {
            // error occured while waiting for trigger or trigger did not arrive
            disarmDevice();

            // perform the register readout and update scalar PVs
            deviceDone();
            readbackParameters();
            snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "timeout while waiting for trigger");
            setStringParam(SIS8300DriverMessage, mMessageBuffer);

            // timeout while waiting for trigger is NOT an error
            if (ret != status_irq_timeout) {
                // leave acquiring state if there was an error!
                mAcquiring = 0;
                snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "%s", "error while waiting for trigger");
                setStringParam(SIS8300DriverMessage, mMessageBuffer);
            }
            for (int addr = 0; addr < maxAddr; addr++) {
                callParamCallbacks(addr);
            }

            // should this be set if an error happened?
            setIntegerParam(SIS8300TriggerTimeout, 1);
            goto taskStart;
        }

        // trigger arrived!
        setIntegerParam(SIS8300TriggerTimeout, 0);

        triggerCount++;

        // obtain the device generated updates in registers
        // usually read-only registers that are not touched by user with register writes
        ret = deviceDone();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }

        ret = acquireArrays();
        if (ret) {
            mAcquiring = 0;
            goto taskStart;
        }

        epicsTimeStamp frameTime;
        epicsTimeGetCurrent(&frameTime);
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        mUniqueId++;

        for (int addr = 0; addr < maxAddr; addr++) {
            callParamCallbacks(addr);
        }

        // notify clients of the new data arrays for enabled channels
        // arrays will have data only if the transfer of the arrays is enabled
        for (int addr = 0; addr < maxAddr; addr++) {
            if (! pArrays[addr]) {
                continue;
            }
            NDArray *pData = pArrays[addr];

            // set the frame number and time stamp
            pData->uniqueId = mUniqueId;
            pData->timeStamp = frameTime.secPastEpoch + frameTime.nsec / 1.e9;
            updateTimeStamp(&pData->epicsTS);

            // get any attributes that have been defined for this driver
            getAttributes(pData->pAttributeList);

            // must release the lock here, or we can get into a deadlock, because we can
            // block on the plugin lock, and the plugin can be calling us
            unlock();
            asynPrintDeviceInfo(pasynUserSelf, "performing doCallbacksGenericPointer for channel %d..", addr);
            doCallbacksGenericPointer(pData, NDArrayData, addr);
            lock();
        }

        getIntegerParam(SIS8300TriggerRepeat, &triggerRepeat);
        asynPrintDeviceInfo(pasynUserSelf, "trigger repeat=%d, trigger count=%d", triggerRepeat, triggerCount);
        // stop the acq if trigger is not set to repeat,
        // or the trigger count has been reached
        if ((triggerRepeat == 0) || ((triggerRepeat > 0) && (triggerCount >= triggerRepeat))) {
            asynPrintDeviceInfo(pasynUserSelf, "stopping acquisition, trigger count %d reached", triggerCount);
            mAcquiring = 0;
        }

    }

    callParamCallbacks(0);
    asynPrintDeviceInfo(pasynUserSelf, "data thread stop");
}

// called when asyn clients call pasynInt32->write()
asynStatus sis8300::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    int ret = 0;
    const char *name;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %d", name, function, value);

    status = setIntegerParam(addr, function, value);

    if (function == ADAcquire) {
        setAcquire(value);
    } else if (function == SIS8300Reset) {
        ret = sis8300drv_master_reset(mDeviceHandle);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_master_reset returned %d", ret);
        }
    } else if (function == SIS8300ClockSource) {
        ret = sis8300drv_set_clock_source(mDeviceHandle, (sis8300drv_clk_src)value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_clock_source returned %d", ret);
        } else {
            updateTickValue();
        }
    } else if (function == SIS8300ClockDivider) {
        ret = sis8300drv_set_clock_divider(mDeviceHandle, (sis8300drv_clk_div)value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_clock_divider returned %d", ret);
        } else {
            updateTickValue();
        }
    } else if (function == SIS8300TriggerSource) {
        ret = sis8300drv_set_trigger_source(mDeviceHandle, (sis8300drv_trg_src)value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_trigger_source returned %d", ret);
        }
    } else if (function == SIS8300TriggerExternalLine) {
        sis8300drv_trg_ext trgext = trg_ext_harlink;
        unsigned int trgmask = (1 << value);
        if (value >= SIS8300DRV_NUM_FP_TRG) {
            trgext = trg_ext_mlvds;
            trgmask = (1 << (value - SIS8300DRV_NUM_FP_TRG));
        }
        ret = sis8300drv_set_external_setup(mDeviceHandle, trgext, trgmask, 0);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_external_setup returned %d", ret);
        }
    } else if (function == SIS8300TriggerDelay) {
        ret = sis8300drv_set_npretrig(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_npretrig returned %d", ret);
        }
    } else if (function == SIS8300ChInternalTriggerLength ||
               function == SIS8300ChInternalTriggerCondition ||
               function == SIS8300ChInternalTriggerOff ||
               function == SIS8300ChInternalTriggerOn) {
        // per channel configuration
        ret = setupInternalTrigger(addr);
    } else if (function == SIS8300ChControl) {
        // per channel configuration
        ret = setupChannelMask(addr);
    } else if (function == SIS8300NumSamples) {
        value &= ~(SIS8300_DMA_ALIGN-1);
        ret = sis8300drv_set_nsamples(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drv_set_nsamples returned %d", ret);
        }
    } else {
        if (function < SIS8300_FIRST_PARAM) {
            status = asynNDArrayDriver::writeInt32(pasynUser, value);
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %d", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    }

    // read back the firmware register values
    // XXX is ignoring the errors here OK?!
    readbackParameters();

    return status;
}

// called when asyn clients call pasynFloat64->write()
asynStatus sis8300::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr;
    const char *name;
    int ret = 0;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %f", name, function, value);

    status = setDoubleParam(addr, function, value);

    if (function == SIS8300SamplingFrequency) {
        updateTickValue();
    } else if (function == SIS8300ChAttenuation) {
        int RTMType;
        getIntegerParam(SIS8300RtmType, &RTMType);
        // only DWC8VM1 and DWC8300-LF have attenuators
        if (((sis8300drv_rtm)RTMType == rtm_dwc8vm1) ||
                ((sis8300drv_rtm)RTMType == rtm_dwc8300lf)) {
            int val = (int)((value + 31.5) * 2);
            // per channel configuration
            ret = sis8300drv_i2c_rtm_attenuator_set(mDeviceHandle, (sis8300drv_rtm)RTMType, addr, val);
            if (ret) {
                asynPrintError(pasynUser, "sis8300drv_i2c_rtm_attenuator_set returned %d", ret);
            }
        }
    } else {
        if (function < SIS8300_FIRST_PARAM) {
            status = asynNDArrayDriver::writeFloat64(pasynUser, value);
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %f", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    }

    // read back the firmware register values
    // XXX is ignoring the errors here OK?!
    readbackParameters();

    return status;
}

// called when asyn clients call pasynOctet->write()
asynStatus sis8300::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual)
{
    int addr;
    int function = pasynUser->reason;
    const char *name;
    asynStatus status = asynSuccess;
    int ret = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %s", name, function, value);

    status = setStringParam(addr, function, (char *)value);

    if (function == SIS8300EvrTimestamp) {
        // remember parsed timestamp values
        ret = sscanf(value, "%u.%u", &mTimeStampSec, &mTimeStampNsec);
        if (ret == 2) {
            // convert to EPICS time (epoch at 1990)
            mTimeStampSec -= POSIX_TIME_AT_EPICS_EPOCH;
            asynPrintDriverInfo(pasynUser, "EPICS time stamp: %u sec %u nsec", mTimeStampSec, mTimeStampNsec);
            ret = 0;
        } else {
            asynPrintError(pasynUser, "sscanf of timestamp returned %d", ret);
            ret = -1;
        }
    } else {
        if (function < SIS8300_FIRST_PARAM) {
            status = asynNDArrayDriver::writeOctet(pasynUser, value, nChars, nActual);
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %s", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    }

    // *not* reading back the firmware register values
    // readbackParameters();

    *nActual = nChars;
    return status;
}

// report status of the driver, prints details about the driver if details>0
void sis8300::report(FILE *fp, int details)
{
    int memorySizeMb;
    char buf[256];

    fprintf(fp, "Port name        : %s\n", portName);
    getStringParam(SIS8300DevicePath, 256, buf);
    fprintf(fp, "Device path      : %s\n", buf);
    getStringParam(ADManufacturer, 256, buf);
    fprintf(fp, "Manufacturer     : %s\n", buf);
    getStringParam(ADFirmwareVersion, 256, buf);
    fprintf(fp, "Firmware version : %s\n", buf);
    getStringParam(ADSerialNumber, 256, buf);
    fprintf(fp, "Serial number    : %s\n", buf);
    getStringParam(ADModel, 256, buf);
    fprintf(fp, "Model            : %s\n", buf);
    getIntegerParam(SIS8300MemorySize, &memorySizeMb);
    fprintf(fp, "Memory size      : %d MB\n", memorySizeMb);

    if (details > 0) {
        int numSamples, dataType;
        getIntegerParam(SIS8300NumSamples, &numSamples);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, "  # samples:       %d\n", numSamples);
        fprintf(fp, "      Data type:   %d\n", dataType);
    }
    // invoke the base class method
    asynNDArrayDriver::report(fp, details);
}

// configuration command, called directly or from iocsh
extern "C" int sis8300Config(const char *portName, const char *devicePath,
        int numSamples, int extraPorts, int maxBuffers, int maxMemory,
        int priority, int stackSize)
{
    new sis8300(portName, devicePath,
            numSamples, extraPorts,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);

    return asynSuccess;
}

// code for iocsh registration
static const iocshArg arg0 = {"Port name",     iocshArgString};
static const iocshArg arg1 = {"Device path",   iocshArgString};
static const iocshArg arg2 = {"Num samples",   iocshArgInt};
static const iocshArg arg3 = {"Extra ports",   iocshArgInt};
static const iocshArg arg4 = {"maxBuffers",    iocshArgInt};
static const iocshArg arg5 = {"maxMemory",     iocshArgInt};
static const iocshArg arg6 = {"priority",      iocshArgInt};
static const iocshArg arg7 = {"stackSize",     iocshArgInt};
static const iocshArg * const configArgs[] = {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6, &arg7};
static const iocshFuncDef config = {"sis8300Configure", 8, configArgs};
static void configCallFunc(const iocshArgBuf *args)
{
    sis8300Config(args[0].sval, args[1].sval, args[2].ival,
        args[3].ival, args[4].ival, args[5].ival, args[6].ival, args[7].ival);
}

static void sis8300Register(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
epicsExportRegistrar(sis8300Register);
}
