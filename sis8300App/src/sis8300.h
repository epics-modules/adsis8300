/* sis8300.h
 *
 * This is a driver for a Struck SIS8300 digitizer.
 * Based on ADCSimDetector ADExample.
 *
 * Author: Hinko Kocevar
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  September 11, 2016
 *
 */

#ifndef _SIS8300_H_
#define _SIS8300_H_

#include <stdint.h>
#include <epicsEvent.h>
#include <epicsTime.h>

#include <asynNDArrayDriver.h>

#include <sis8300drv.h>

// board strings
#define SIS8300DriverMessageString                      "SIS8300.DRIVER_MESSAGE"
#define SIS8300NumSamplesString                         "SIS8300.NUM_SAMPLES"
#define SIS8300ClockSourceString                        "SIS8300.CLOCK_SOURCE"
#define SIS8300ClockDividerString                       "SIS8300.CLOCK_DIVIDER"
#define SIS8300TriggerSourceString                      "SIS8300.TRIGGER_SOURCE"
#define SIS8300TriggerLineString                        "SIS8300.TRIGGER_EXTERNAL_LINE"
#define SIS8300TriggerDelayString                       "SIS8300.TRIGGER_DELAY"
#define SIS8300TriggerRepeatString                      "SIS8300.TRIGGER_REPEAT"
#define SIS8300IrqPollString                            "SIS8300.IRQ_POLL"
#define SIS8300ResetString                              "SIS8300.RESET"
#define SIS8300DevicePathString                         "SIS8300.DEVICE_PATH"
#define SIS8300MemorySizeString                         "SIS8300.MEMORY_SIZE"
#define SIS8300RtmTypeString                            "SIS8300.RTM_TYPE"
#define SIS8300SamplingFrequencyString                  "SIS8300.SAMPLING_FREQUENCY"
#define SIS8300TickValueString                          "SIS8300.TICK_VALUE"
#define SIS8300BeamModeSourceString                     "SIS8300.BEAM_MODE.SOURCE"
#define SIS8300BeamModeValueString                      "SIS8300.BEAM_MODE.VALUE"
#define SIS8300BeamDestinationSourceString              "SIS8300.BEAM_DESTINATION.SOURCE"
#define SIS8300BeamDestinationValueString               "SIS8300.BEAM_DESTINATION.VALUE"
#define SIS8300PcieLinkSpeedString                      "SIS8300.PCIE_LINK_SPEED"
#define SIS8300PcieLinkWidthString                      "SIS8300.PCIE_LINK_WIDTH"
#define SIS8300PcieTransferTimeString                   "SIS8300.PCIE_TRANSFER_TIME"
#define SIS8300PcieStartTimeString                      "SIS8300.PCIE_START_TIME"
#define SIS8300PcieEndTimeString                        "SIS8300.PCIE_END_TIME"
#define SIS8300TransferArraysString                     "SIS8300.TRANSFER_ARRAYS"
#define SIS8300TriggerTimeoutString                     "SIS8300.TRIGGER_TIMEOUT"
#define SIS8300TriggerTimeoutValueString                "SIS8300.TRIGGER_TIMEOUT_VALUE"
#define SIS8300ADCRangeString                           "SIS8300.ADC_RANGE"
#define SIS8300ConvertToVoltageString                   "SIS8300.CONVERT_TO_VOLTAGE"
// EVR related strings
#define SIS8300EvrTimestampString                       "SIS8300.EVR.TIMESTAMP"
#define SIS8300EvrBeamStateString                       "SIS8300.EVR.BEAM_STATE"
#define SIS8300EvrLinkString                            "SIS8300.EVR.LINK"
#define SIS8300EvrLinkStatusString                      "SIS8300.EVR.LINK_STATUS"
#define SIS8300EvrLinkSeverityString                    "SIS8300.EVR.LINK_SEVERITY"
// channel strings
#define SIS8300ChControlString                          "SIS8300.CH.CONTROL"
#define SIS8300ChAttenuationString                      "SIS8300.CH.ATTENUATION"
#define SIS8300ChInternalTriggerLengthString            "SIS8300.CH.INTERNAL_TRIGGER_LENGTH"
#define SIS8300ChInternalTriggerConditionString         "SIS8300.CH.INTERNAL_TRIGGER_CONDITION"
#define SIS8300ChInternalTriggerOffString               "SIS8300.CH.INTERNAL_TRIGGER_OFF"
#define SIS8300ChInternalTriggerOnString                "SIS8300.CH.INTERNAL_TRIGGER_ON"
#define SIS8300ChPcieTransferTimeString                 "SIS8300.CH.PCIE_TRANSFER_TIME"
#define SIS8300ChPcieStartTimeString                    "SIS8300.CH.PCIE_START_TIME"

#define MAX_PATH_LENGTH                32
#define MAX_MESSAGE_LENGTH             256

// TODO: set error message
// epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
//           "%s:%s: status=%d, function=%d, paramName=%s, value=%s",
//           driverName, functionName, status, function, name, value);

#define asynPrintError(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_ERROR, \
            "%s::%s [ERROR] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDeviceInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DEVICE, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDriverInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintFlow(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_FLOW, \
            "%s::%s [FLOW] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

// Struck SIS8300 L/L2/KU driver
class epicsShareClass sis8300 : public asynNDArrayDriver {
public:
    sis8300(const char *portName, const char *devicePath,
            int numSamples, int extraPorts, int maxBuffers, size_t maxMemory,
            int priority, int stackSize);
    virtual ~sis8300();

    // these are the methods that we override from asynNDArrayDriver
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);
    virtual void report(FILE *fp, int details);
    // should be private, but gets called from C, so must be public
    void sisTask();

protected:
    // board parameters
    int SIS8300NumSamples;
    #define SIS8300_FIRST_PARAM SIS8300NumSamples
    int SIS8300ClockSource;
    int SIS8300ClockDivider;
    int SIS8300TriggerSource;
    int SIS8300TriggerExternalLine;
    int SIS8300TriggerDelay;
    int SIS8300TriggerRepeat;
    int SIS8300IrqPoll;
    int SIS8300Reset;
    int SIS8300DriverMessage;
    int SIS8300DevicePath;
    int SIS8300MemorySize;
    int SIS8300RtmType;
    int SIS8300SamplingFrequency;
    int SIS8300TickValue;
    int SIS8300BeamModeSource;
    int SIS8300BeamModeValue;
    int SIS8300BeamDestinationSource;
    int SIS8300BeamDestinationValue;
    int SIS8300PcieLinkSpeed;
    int SIS8300PcieLinkWidth;
    int SIS8300PcieTransferTime;
    int SIS8300PcieStartTime;
    int SIS8300PcieEndTime;
    int SIS8300TransferArrays;
    int SIS8300TriggerTimeout;
    int SIS8300TriggerTimeoutValue;
    int SIS8300ADCRange;
    int SIS8300ConvertToVoltage;
    // EVR parameters
    int SIS8300EvrTimestamp;
    int SIS8300EvrBeamState;
    int SIS8300EvrLink;
    int SIS8300EvrLinkStatus;
    int SIS8300EvrLinkSeverity;
    // channel parameters
    int SIS8300ChControl;
    int SIS8300ChAttenuation;
    int SIS8300ChInternalTriggerLength;
    int SIS8300ChInternalTriggerCondition;
    int SIS8300ChInternalTriggerOff;
    int SIS8300ChInternalTriggerOn;
    int SIS8300ChPcieTransferTime;
    int SIS8300ChPcieStartTime;

    // these are the methods that are new to this class
    int acquireRawArrays();
    template <typename epicsType> int convertArraysT();
    virtual int acquireArrays();
    void setAcquire(int value);
    virtual int initDevice();
    virtual int destroyDevice();
    virtual int initDeviceDone();
    virtual int armDevice();
    virtual int disarmDevice();
    virtual int waitForDevice();
    virtual int deviceDone();
    virtual int readbackParameters();
    virtual int setupChannelMask(int addr);
    virtual int setupInternalTrigger(int addr);
    virtual void updateTickValue();
    virtual bool checkEvrLink();

    // these members can be used by derived classes
    sis8300drv_usr *mDeviceHandle;
    char mMessageBuffer[MAX_MESSAGE_LENGTH];

private:
    epicsEventId mStartEventId;
    epicsEventId mStopEventId;
    int mUniqueId;
    int mAcquiring;
    char mDevicePath[MAX_PATH_LENGTH];
    bool mExitAcqThread;
    epicsThreadId mAcqThreadId;

public:
    // these two should be protected but need to be public since called from
    // myTimeStampSource()
    epicsUInt32 mTimeStampSec;
    epicsUInt32 mTimeStampNsec;
    epicsTimeStamp mInterruptTs;
};

#endif /* _SIS8300_H_ */

